# Laravel 7 Admin LTE 3 Integration
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-7-admin-lte-web.git`
2. Go inside the folder: `cd laravel-7-admin-lte-web`
3. Run `cd .env.example .env` and add your desired database name, database username & password
4. Run `composer install`
5. Run `php artisan migrate`
6. Run `php artisan serve`

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Register Page

![Register Page](img/register.png "Register Page")

Login Page

![Login Page](img/login.png "Login Page")

Dashboard Page

![Dashboard Page](img/dashboard.png "Dashboard Page")


For more details, please visit this [link](https://github.com/jeroennoten/Laravel-AdminLTE#2-installation).
